#ifndef CUSTOMER_H
#define CUSTOMER_H

#include <vector>
#include <iostream>
#include <sstream>
#include "Pipe.h"

class Customer
{
public:
    Customer(int, int, int, int, int);
    void addCommand(const std::string&);
    bool hasNextCommand()const;
    void sendCommand(Pipe&, time_t)const;
    void nextTransaction();
    int getCustomerId()const { return cid; }
    int getReserveTime()const { return reserveTime; }
    int getCancelTime()const { return cancelTime; }
    int getCheckTime()const { return checkTime; }
    int getPayTime()const { return payTime; }
    friend std::ostream& operator<<(std::ostream&, const Customer&);
private:
    int cid, reserveTime, cancelTime, checkTime, payTime;
    std::vector<std::string> commands;
    size_t cmdId;
};

#endif
