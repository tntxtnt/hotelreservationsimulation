#include "HotelTransactionHandler.h"


int checkReservationConflict(const Transaction& t, const HotelRooms& hotelRooms)
{
    const int nRooms = hotelRooms.size() - 1;
    for (size_t roomIter = 0; roomIter < t.getRooms().size(); ++roomIter)
    {
        int rid = t.getRooms()[roomIter];
        RoomReservation res(rid, t.getStartDate(), t.getDuration(), t.getCustomerId());
        if (rid > nRooms || !isReservable(res, hotelRooms[rid]))
            return rid;
    }
    return 0;
}

TransactionResult handleReservation(const Transaction& t, HotelRooms& rooms)
{
    std::ostringstream resultStream;
    const int nRooms = rooms.size() - 1;
    int conflictRoomId = checkReservationConflict(t, rooms);
    if (!conflictRoomId)
    {
        for (size_t roomIter = 0; roomIter < t.getRooms().size(); ++roomIter)
        {   //foreach room in rooms, insert reservation
            int rid = t.getRooms()[roomIter];
            RoomReservation res(rid, t.getStartDate(), t.getDuration(), t.getCustomerId());
            rooms[rid][res.startDate] = res;
        }
        resultStream << "Reservation completed.";
    }
    else if (conflictRoomId > nRooms)
    {
        resultStream << "ERROR: Our hotel has no such room " << conflictRoomId
                     << ". Transaction cancelled.";
    }
    else
    {
        resultStream << "CONFLICT: Reservation on room " << conflictRoomId
                     << " cannot be done. Transaction cancelled.";
    }
    return TransactionResult(t, resultStream.str());
}

int checkCancellationConflict(const Transaction& t, const HotelRooms& hotelRooms)
{
    const int nRooms = hotelRooms.size() - 1;
    for (size_t roomIter = 0; roomIter < t.getRooms().size(); ++roomIter)
    {
        int rid = t.getRooms()[roomIter];
        RoomReservation res(rid, t.getStartDate(), t.getDuration(), t.getCustomerId());
        if (rid > nRooms || !isCancellable(res, hotelRooms[rid]))
            return rid;
    }
    return 0;
}

TransactionResult handleCancellation(const Transaction& t, HotelRooms& rooms)
{
    std::ostringstream resultStream;
    const int nRooms = rooms.size() - 1;
    int conflictRoomId = checkCancellationConflict(t, rooms);
    if (!conflictRoomId)
    {
        for (size_t roomIter = 0; roomIter < t.getRooms().size(); ++roomIter)
        {   //foreach room in rooms, erase reservation
            int rid = t.getRooms()[roomIter];
            rooms[rid].erase(t.getStartDate());
        }
        resultStream << "Cancellation completed.";
    }
    else if (conflictRoomId > nRooms)
    {
        resultStream << "ERROR: Our hotel has no such room " << conflictRoomId
                     << ". Transaction cancelled.";
    }
    else
    {
        resultStream << "CONFLICT: Cancellation on room " << conflictRoomId
                     << " cannot be done. Transaction cancelled.";
    }
    return TransactionResult(t, resultStream.str());
}

TransactionResult handleChecking(const Transaction& t, const HotelRooms& rooms,
                                 const StringVec& custNames)
{
    std::ostringstream resultStream;
    std::map<int,RoomReservation>::const_iterator it;
    int reservationCount = 0;
    // foreach room in hotelRooms
    for (size_t rid = 1; rid < rooms.size(); ++rid)
    {
        // for each reservation in room (hotelRooms[rid])
        for (it = rooms[rid].begin(); it != rooms[rid].end(); ++it)
        {
            if (it->second.cid == t.getCustomerId())
            {
                std::cout << "\n";
                print(std::cout, it->second, custNames);
                reservationCount++;
            }
        }
    }
    if (!reservationCount) std::cout << "\n";
    std::cout << "You have " << reservationCount << " reservation(s).\n";
    resultStream << "Check completed.";
    return TransactionResult(t, resultStream.str());
}

int checkPaymentConflict(const Transaction& t, const HotelRooms& hotelRooms)
{
    const int nRooms = hotelRooms.size() - 1;
    for (size_t roomIter = 0; roomIter < t.getRooms().size(); ++roomIter)
    {
        int rid = t.getRooms()[roomIter];
        RoomReservation res(rid, t.getStartDate(), t.getDuration(), t.getCustomerId());
        if (rid > nRooms || !isPayable(res, hotelRooms[rid]))
            return rid;
    }
    return 0;
}

TransactionResult handlePayment(const Transaction& t, HotelRooms& rooms)
{
    std::ostringstream resultStream;
    const int nRooms = rooms.size() - 1;
    int conflictRoomId = checkPaymentConflict(t, rooms);
    if (!conflictRoomId)
    {
        for (size_t roomIter = 0; roomIter < t.getRooms().size(); ++roomIter)
        {   //foreach room in rooms, insert reservation
            int rid = t.getRooms()[roomIter];
            RoomReservation res(rid, t.getStartDate(), t.getDuration(), t.getCustomerId());
            rooms[rid][res.startDate].paid = true;
        }
        resultStream << "Payment completed.";
    }
    else if (conflictRoomId > nRooms)
    {
        resultStream << "ERROR: Our hotel has no such room " << conflictRoomId
                     << ". Transaction cancelled.";
    }
    else
    {
        resultStream << "CONFLICT: Payment on room " << conflictRoomId
                     << " cannot be done. Transaction cancelled.";
    }
    return TransactionResult(t, resultStream.str());
}
