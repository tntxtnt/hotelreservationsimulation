# EXECUTABLES - DO NOT MODIFIED
CXX = g++
RM = rm -rf
MKDIR = mkdir -p

# HELPFUL COMMANDS
print-%  : ; @echo $* = $($*)

# PROJECT NAME - will be output executable name
PROJECT = hotel


# Include
INC_DIRS = 

# Linker
LIB_DIRS = 


# BUILD TARGETS - change your compile flags here
BUILD_RELEASE = Release
CFLAGS_RELEASE = -std=c++14 -O2
LFLAGS_RELEASE = -s

# Add another build target here

# Switch your build target here
BUILD = $(BUILD_RELEASE)
CFLAGS = -Wall $(CFLAGS_RELEASE)
LFLAGS = $(LFLAGS_RELEASE)


# Input directory
SRCDIR = .

# Modules
MODULES = .

# Output directories - DO NOT MODIFIED
OBJDIR = build/$(BUILD)/obj
EXEDIR = build/$(BUILD)/bin
DEPDIR = build/dependencies
EXE = $(EXEDIR)/$(PROJECT)
EXEWIN = build\$(BUILD)\bin\$(PROJECT).exe

$(EXEDIR):
	$(MKDIR) $(EXEDIR)

TEMPALL_OBJS_DIRS += $(foreach module, $(MODULES), $(OBJDIR)/$(module))
ALL_OBJS_DIRS = $(TEMPALL_OBJS_DIRS:%.=%)
$(ALL_OBJS_DIRS):
	$(MKDIR) $@

TEMP_ALL_DEPS_DIRS +=  $(foreach module, $(MODULES), $(DEPDIR)/$(module))
ALL_DEPS_DIRS = $(TEMP_ALL_DEPS_DIRS:%.=%)
$(ALL_DEPS_DIRS):
	$(MKDIR) $@

# .cpp, .h, .o, .d
RSRCS = $(foreach module, $(MODULES), $(wildcard $(module)/*.cpp))
RSRCS_NODOT = $(RSRCS:./%=%)
RDEPS = $(RSRCS_NODOT:%.cpp=$(DEPDIR)/%.d)
ROBJS = $(RSRCS_NODOT:%.cpp=$(OBJDIR)/%.o)

$(DEPDIR)/%.d: $(SRCDIR)/%.cpp | $(ALL_DEPS_DIRS)
	$(CXX) $< -MM -MT "$(<:%.cpp=$(OBJDIR)/%.o)" > $@
	$(CXX) $< -MM -MT $@ >> $@

-include $(RDEPS)

$(OBJDIR)/%.o: $(SRCDIR)/%.cpp | $(ALL_OBJS_DIRS)
	$(CXX) -c $(CFLAGS) $< -o $@

	
build: $(ROBJS) | $(EXEDIR)
	$(CXX) $(ROBJS:%/mainEDF.o=)  $(LFLAGS) -o $(EXE)_fcfs
	$(CXX) $(ROBJS:%/mainFCFS.o=) $(LFLAGS) -o $(EXE)_edf
	
run:
	start cmd /k "echo %time%  && $(EXEWIN) && echo. && pause && exit"

clean:
	$(RM) $(ROBJS)

.PHONY: run build clean