#include "RoomReservation.h"


bool RoomReservation::similar(const RoomReservation& other)const
{
    return this->startDate == other.startDate
        && this->duration == other.duration
        && this->cid == other.cid
        && this->rid == other.rid;
}

bool isReservable(const RoomReservation& res, const std::map<int,RoomReservation>& rooms)
{
    if (rooms.empty())
        return true;
    std::map<int,RoomReservation>::const_iterator it = rooms.lower_bound(res.startDate);
    // check conflict at right end
    if (it != rooms.end() && res.endDate() > it->second.startDate)
        return false;
    // left end
    if (it == rooms.begin())
        return true;
    else if ((--it)->second.endDate() > res.startDate)
        return false;
    // no conflict
    return true;
}

bool isPayable(const RoomReservation& res, const std::map<int,RoomReservation>& rooms)
{
    if (rooms.empty())
        return false;
    const RoomReservation& room = rooms.lower_bound(res.startDate)->second;
    if (!res.similar(room)) // cannot pay unreserved room
        return false;
    if (room.paid) //don't pay twice!
        return false;
    return true;
}

bool isCancellable(const RoomReservation& res, const std::map<int,RoomReservation>& rooms)
{
    if (rooms.empty())
        return false;
    const RoomReservation& room = rooms.lower_bound(res.startDate)->second;
    if (!res.similar(room)) // cannot cancel unreserved room
        return false;
    if (room.paid) // cannot cancel paid room!
        return false;
    return true;
}

void print(std::ostream& out, const RoomReservation& res,
           const std::vector<std::string>& custNames)
{
    out << "  Rooms: " << res.rid << "\n"
        << "  StartDate: " << res.startDate << "\n"
        << "  Duration: " << res.duration << "\n"
        << "  CustomerName: " << custNames[res.cid] << "\n"
        << "  " << (res.paid ? "" : "UN") << "PAID\n";
}
