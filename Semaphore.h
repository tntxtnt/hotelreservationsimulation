#ifndef SEMAPHORE_H
#define SEMAPHORE_H

#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <cstdlib>
#include <vector>
#include <iostream>

class Semaphore
{
public:
    void create(int key, int semnum, int nsops);
    void setSemValue(int semnum, int value);
    void prepareWait(int i, int semnum);
    void prepareSignal(int i, int semnum);
    void run(int count);
    void release();
private:
    int semid;
    int nsops;
    std::vector<sembuf> sb;
};

#endif
