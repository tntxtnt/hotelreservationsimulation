#include "Transaction.h"

Transaction::Transaction()
: type(None), rooms(), startDate(-1), duration(-1), cid(-1), customerName(),
  softDeadline(-1), hardDeadline(-1)
{
}

Transaction::Transaction(const std::string& line)
: type(None), rooms(), startDate(-1), duration(-1), cid(-1), customerName(),
  softDeadline(-1), hardDeadline(-1)
{
    parse(line);
}

void Transaction::reset()
{
    type = None;
    rooms.clear();
    startDate = duration = cid = softDeadline = hardDeadline = -1;
}

int Transaction::parseDeadline(const std::string& command)
{
    return atoi(command.substr(command.rfind(' ') + 1).c_str());
}

void Transaction::parse(const std::string& command)
{
    std::istringstream iss(command);
    std::string token;

    iss >> token;
    if      (token.find("reserve") != token.npos) type = Reserve;
    else if (token.find("cancel") != token.npos)  type = Cancel;
    else if (token.find("check") != token.npos)   type = Check;
    else if (token.find("pay") != token.npos)     type = Pay;

    if (type == Check)
    {
        iss >> customerName >> token;
        if (token == "deadline") iss >> softDeadline;
    }
    else
    {
        if (token.find('(') != token.npos)
            token = token.substr(token.find('('));
        else
            iss >> token;
        if (token.find('-') != token.npos)
        {
            int startRoomNum = atoi(&token[1]);
            int endRoomNum = atoi(&token[token.find('-')+1]);
            for (int i = startRoomNum; i <= endRoomNum; ++i)
                rooms.push_back(i);
        }
        else if (token.find(',') != token.npos)
        {
            std::istringstream roomIss(&token[1]);
            while (std::getline(roomIss, token, ','))
                rooms.push_back(atoi(token.c_str()));
        }
        else
        {
            rooms.push_back(atoi(&token[1]));
        }

        iss >> startDate >> duration >> customerName >> token;
        if (token == "deadline") iss >> softDeadline;
    }
    iss >> cid >> hardDeadline; //hardDeadline == issueTime
    hardDeadline += softDeadline;
}

std::ostream& operator<<(std::ostream& out, const Transaction& t)
{
    out << "  Type: ";
    if (t.type == Transaction::Reserve)
        out << "reserve\n";
    else if (t.type == Transaction::Cancel)
        out << "cancel\n";
    else if (t.type == Transaction::Check)
        out << "check\n";
    else if (t.type == Transaction::Pay)
        out << "pay\n";

    if (t.type != Transaction::Check)
    {
        out << "  Rooms: ";
        for (size_t i = 0; i < t.rooms.size(); ++i)
            out << t.rooms[i] << " ";
        out << "\n";
        out << "  StartDate: " << t.startDate << "\n";
        out << "  Duration: " << t.duration << "\n";
    }
    return out << "  CustomerName: " << t.customerName << "\n"
               << "  SoftDeadline: " << t.softDeadline << "\n";
}
