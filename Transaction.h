#ifndef TRANSACTION_H
#define TRANSACTION_H

#include <vector>
#include <sstream>
#include <cstdlib>
#include <iostream>

class Transaction
{
public:
    enum Type { None, Reserve, Cancel, Check, Pay };
public:
    Transaction();
    Transaction(const std::string& line);
    void parse(const std::string& command);
    void reset();
    int getType()const { return type; }
    int getStartDate()const { return startDate; }
    int getDuration()const { return duration; }
    const std::vector<int>& getRooms()const { return rooms; }
    int getCustomerId()const { return cid; }
    const std::string& getCustomerName()const { return customerName; }
    int getDeadline()const { return softDeadline; }
    int getHardDeadline()const { return hardDeadline; }
    void setHardDeadline(int D) { hardDeadline = D; }
    friend std::ostream& operator<<(std::ostream&, const Transaction&);
    static int parseDeadline(const std::string&);
private:
    int type;
    std::vector<int> rooms;
    int startDate;
    int duration;
    int cid;
    std::string customerName;
    int softDeadline;
    time_t hardDeadline;
};

#endif
