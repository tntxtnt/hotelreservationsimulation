#include <iostream>
#include <fstream>
#include "Customer.h"
#include "Semaphore.h"
#include "Pipe.h"
#include "HotelTransactionHandler.h"
#include <ctime>

const int KEY = 1017823;
const int HOTEL_SEM = 0;

void safeOpen(std::ifstream&, const std::string&);
void readInput(int&, int&, std::vector<Customer>&);

void hotelServer(Semaphore&, Pipe&, int&, StringVec&,
                 const std::vector<Customer>&, HotelRooms&, TransactionLog&);
void customerClient(Semaphore&, Pipe&, Customer&);

int main()
{
    int nRooms, nCustomers;
    std::vector<Customer> customers;
    Semaphore sem;
    Pipe pipe;
    int customerId = 0;
    int pid = -1;
    StringVec customerNames;
    HotelRooms hotelRooms;
    TransactionLog transactionLog;

    // Read input
    readInput(nRooms, nCustomers, customers);
    customerNames.resize(nCustomers + 1, "");
    hotelRooms.resize(nRooms + 1);

    // Create pipe & sem and set sems' initial values
    pipe.create();
    sem.create(KEY, 1, 1);
    sem.setSemValue(HOTEL_SEM, 1);

    // Fork all customers
    for (customerId = 0; customerId < nCustomers; ++customerId)
    {
        if ((pid = fork()) == -1)
        {
            std::cout << "Fork error.\n";
            exit(1);
        }
        else if (pid == 0)
            break;
    }

    //////////////////// HOTEL SIMULATION ////////////////////

    //------------------------------------------------------//
    //---------------------- Customers ---------------------//
    //------------------------------------------------------//
    if (pid == 0)
    {
        customerClient(sem, pipe, customers[customerId]);
        exit(0);
    }

    //------------------------------------------------------//
    //----------------------   Hotel  ----------------------//
    //------------------------------------------------------//
    hotelServer(sem, pipe, nCustomers, customerNames, customers, hotelRooms,
                transactionLog);
    // Print reports
    reportTransactionLog(transactionLog);
    reportRoomStatus(hotelRooms, customerNames);
    reportDeadlines(transactionLog);
}



void customerClient(Semaphore& sem, Pipe& pipe, Customer& customer)
{
    pipe.closeReadEnd();
    while (customer.hasNextCommand())
    {
        time_t issueTime = time(NULL);

        // WAIT(hotel_sem)
        sem.prepareWait(0, HOTEL_SEM); //set op 0 to wait on TRANSAC_SEM
        sem.run(1); //run only first op (wait)

        customer.sendCommand(pipe, issueTime);
        customer.nextTransaction();
    }
    pipe.closeWriteEnd();
}

void hotelServer(Semaphore& sem, Pipe& pipe, int& nCustomers, StringVec& custNames,
                 const std::vector<Customer>& customers, HotelRooms& hotelRooms,
                 TransactionLog& transacLog)
{
    pipe.closeWriteEnd();
    time_t timer = time(NULL);
    while (nCustomers)
    {
        char buf[80];
        int nread = read(pipe.readEnd(), buf, 80);
        buf[nread] = '\0';
        std::string command = buf;

        if (command.find("END") != command.npos)
        {
            --nCustomers;
        }
        else
        {
            Transaction t = Transaction(command);

            std::cout << "Processing transaction #" << transacLog.size() + 1 << "... ";
            std::cout.flush();

            // register 'new' customer (if new customer)
            if (custNames[t.getCustomerId()].empty())
            custNames[t.getCustomerId()] = t.getCustomerName();

            // process transaction + simulate the execution time
            const Customer& customer = customers[t.getCustomerId() - 1];
            TransactionResult tRes;
            int execTime = 0;
            switch (t.getType())
            {
            case Transaction::Reserve:
                execTime = customer.getReserveTime();
                tRes = handleReservation(t, hotelRooms);
                break;
            case Transaction::Cancel:
                execTime = customer.getCancelTime();
                tRes = handleCancellation(t, hotelRooms);
                break;
            case Transaction::Check:
                execTime = customer.getCheckTime();
                tRes = handleChecking(t, hotelRooms, custNames);
                break;
            case Transaction::Pay:
                execTime = customer.getPayTime();
                tRes = handlePayment(t, hotelRooms);
                break;
            default:
                break;
            }
            sleep(execTime);
            timer = time(NULL);
            tRes.finishTime = timer;
            transacLog.push_back(tRes);
            std::cout << "Done!\n";
        }

        // SIGNAL(transaction_sem)
        sem.prepareSignal(0, HOTEL_SEM); //set op 0 to signal on TRANSAC_SEM
        sem.run(1); //run only first op (signal)
    }
    pipe.closeReadEnd();
    sem.release();
}

void readInput(int& nRooms, int& nCustomers, std::vector<Customer>& customers)
{
    std::ifstream fin;
    safeOpen(fin, "hw2input.txt");
    fin >> nRooms >> nCustomers;
    fin.ignore(100, '\n');
    for (int i = 1; i <= nCustomers; ++i)
    {
        std::string line;
        fin.ignore(100, '\n');
        int reserveTime, cancelTime, checkTime, payTime;
        fin >> line >> reserveTime >> line >> cancelTime
            >> line >> checkTime >> line >> payTime;
        fin.ignore(100, '\n');
        Customer customer(i, reserveTime, cancelTime, checkTime, payTime);
        while (std::getline(fin, line) && line.find("end") == line.npos)
        {
            customer.addCommand(line);
        }
        customer.addCommand("END"); ///////////
        customers.push_back(customer);
        fin.ignore(100, '\n');
    }
    fin.close();
}

void safeOpen(std::ifstream& fin, const std::string& fpath)
{
    fin.open(fpath.c_str());
    if (!fin)
    {
        std::cout << "Cannot open file " << fpath << "\n";
        exit(1);
    }
}

